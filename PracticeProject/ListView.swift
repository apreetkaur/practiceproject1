//
//  ListView.swift
//  PracticeProject
//
//  Created by neeraj bala on 2/12/21.
//

import UIKit

class ListView: UIViewController
{
    
    @IBOutlet weak var tableViewNew: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewNew.register(UINib(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "ItemCell")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
